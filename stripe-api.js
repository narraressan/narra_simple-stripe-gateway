const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
	res.render('index') // this is where you create the source. A source is created in client side
})

let stripeCustomer = require('./services/stripeCustomer')
app.get('/stripe/customer', stripeCustomer.createCustomer)

let stripeCharge = require('./services/stripeCharge')
app.post('/stripe/charge', stripeCharge.createCharge)

app.listen(80)