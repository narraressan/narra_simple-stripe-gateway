const config = require('./config.js')

module.exports = {
	createCustomer: function(req, res) {
		try {
			let params = req.query
			const stripe = config.gateway

			stripe.customers.create({
				email: params.email
			})
			.then((customer) => { res.json(customer) })
			.catch((error) => { res.status(400).json({ error: 'request-failed' }) })
		}
		catch(err){ res.status(400).json({ error: 'process-failed' }) }
	}
}