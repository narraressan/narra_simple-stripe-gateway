const config = require('./config.js')

module.exports = {
	// 4242 4242 4242 4242
	createCharge: function(req, res) {
		try {
			let data = req.body
			const stripe = config.gateway

			stripe.customers.createSource(data.customer_id, {
				source: data.source_id
			})
			.then(function(source) {
				return stripe.charges.create({
					customer: source.customer,
					currency: 'usd',
					amount: 50, // $0.50 in cents
					description: data.service + ': ' + data.description
				})
			})
			.then((charge) => { res.json(charge) })
			.catch((error) => { res.status(400).json({ error: 'request-failed' }) })
		}
		catch(err){ res.status(400).json({ error: 'process-failed' }) }
	}
}