### Simple Stripe App
This app is a simple back and frontend implementation of stripe payment gateway.
This aims to give a general idea on how to implement a complete stripe payment gateway.

#### General Guide
1. Repository structure
    - **/stripe-api.js** - main stripe API
    - **/services** - inclused all functions for processing stripe transactions *(e.g. create customer, pay/charge)*
    - **/public** - contains *index.html* for serving sample payment UI


2. Installation
    - execute `npm install --verbose` to install npm modules


2. Run Test
    ```sh
    $ nodemon stripe-api.js
    ```